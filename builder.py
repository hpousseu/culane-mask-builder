#!/usr/bin/python3

import os

from PIL import Image 
from PIL import ImageDraw

from glob import glob

PATH_CULANE_DATA = "/home/hugo/Documents/.local.thesis/project/data/culane"

class BuilderImage:
    """
    Build mask from image and lines txt 
    """

    LINE_WIDTH = 10

    def __init__(self,path_image,path_lines_txt): 
        self._path_image = path_image
        self._path_lines_txt = path_lines_txt

    def _load_img(self,path_image): 
        return Image.open(path_image)

    def _load_txt(self,path_lines_txt): 
        data = None
        with open(path_lines_txt, 'r') as file:
            data = file.read().split('\n')
        data.remove('')
        return data
    
    def _convert_lines_str_to_float(self,lines_str):
        lines_float = [] 
        for line in lines_str:
            lines_float.append(list(map(float,line.strip().split(' '))))
        return lines_float

    def _create_black_img(self,image): 
        return Image.new('RGB', (image.size[0], image.size[1]))

    def _draw_lines_on_image(self,image,lines): 
        draw = ImageDraw.Draw(image)
        for line in lines: 
            draw.line(line,fill=255,width=BuilderImage.LINE_WIDTH)
        return image

    def build(self,is_mask=False): 
        # load
        image = self._load_img(self._path_image)
        lines_txt = self._load_txt(self._path_lines_txt)
        # convert 
        if(is_mask):
            image = self._create_black_img(image)
        lines_float = self._convert_lines_str_to_float(lines_txt) 
        # draw
        image_with_lines = self._draw_lines_on_image(image,lines_float)
        return image_with_lines

class PathExtractor:
    """
    Extract and create paths from path image
    """

    PATTERN_SUFFIX = "_label"
    IMAGE_EXTENSION = "jpg"

    def __init__(self,path_image): 
        self._path_image = path_image

    def _extract_name(self,path):
        basename = os.path.basename(path)
        filename_wo_ext = os.path.splitext(basename)[0]
        return filename_wo_ext

    def _extract_folder(self,path):
        return os.path.dirname(path)

    def _find_lines(self,path,folder_parent,filename_wo_ext): 
        path_txt = glob(f"{folder_parent}/*{filename_wo_ext}*.txt")
        return path_txt[0]

    def _create_label_path(self,folder_parent,filename_wo_ext,pattern_suffix,extension_image): 
        return f"{folder_parent}/{filename_wo_ext}{pattern_suffix}.{extension_image}"

    def extract(self):
        # extract information from path_image
        filename_wo_ext = self._extract_name(self._path_image)
        folder_parent = self._extract_folder(self._path_image)
        # find lines_txt file
        path_lines_txt = self._find_lines(self._path_image,folder_parent,filename_wo_ext)
        # create mask image label
        path_image_label = self._create_label_path(folder_parent,filename_wo_ext,PathExtractor.PATTERN_SUFFIX,PathExtractor.IMAGE_EXTENSION)
        return path_lines_txt, path_image_label


def build(path_culane,is_mask=True): 
    """
    handle all pipeline from paths extraction to mask image generation
    Args:
        path_culane (str): root path of the CuLane dataset
    """
    pattern = f"{path_culane}/**/*[!{PathExtractor.PATTERN_SUFFIX}].jpg"
    paths_image = glob(pattern,recursive=True)
    for path_image in paths_image: 
        path_lines_txt, path_image_label = PathExtractor(path_image=path_image).extract()
        image_mask = BuilderImage(path_image=path_image,path_lines_txt=path_lines_txt).build(is_mask=is_mask)
        image_mask.save(path_image_label)

def debug():
    # -- test varibales -- 
    path_test_mask = "./test/mask"
    path_test_on_image = "./test/on_image"
    build(path_test_mask,is_mask=True)
    build(path_test_on_image,is_mask=False)

def release():
    # -- pipeline -- 
    build(PATH_CULANE_DATA)

if __name__ == "__main__":
   release()
   
    
