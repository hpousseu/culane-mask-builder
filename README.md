# culane-mask-builder

## Goal

Create line mask from image and lines description of the CuLane dataset: https://xingangpan.github.io/projects/CULane.html 

### example: 

![EXAMPLE-IMG](./test/mask/00540.jpg)

- extract only the mask 

![EXAMPLE-IMG-MASK](./test/mask/00540_label.jpg)

- add the mask on the initial image 

![EXAMPLE-IMG-MASK](./test/on_image/00540_label.jpg)

## Setup

script: Python3

package: PIL, os, glob

## Start

1. download CuLane dataset and extract archives somewhere in your computer (=PATH_CULANE_DATA)
2. update variable inside the script: "PATH_CULANE_DATA" = with the absolute path where CuLane dataset is saved
3. run the script: python3 builder.py